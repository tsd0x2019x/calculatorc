/**
 * Calculator.h
 *
 */
#ifndef CALCULATOR_H
#define CALCULATOR_H

#ifdef __cplusplus
extern "C" {
#endif

//=======================================================================================================================
#include <stdio.h>
#include "Parser.h"
#define CALCULATOR_API extern;
#define LEN 101 // Length of input (expression). Including null-terminated character '\0'.

typedef struct { 
    char buffer[LEN];
    Parser parser;
 } Calculator;

CALCULATOR_API void Calculator_Init (Calculator * calculator) {
    for (int i = 0; i < LEN; ++i)
        calculator->buffer[i] = 0;
}

CALCULATOR_API void Calculator_Run (Calculator * calculator) {
    //// Read expression (array of chars) from stdin
    //
    char * expression = fgets(calculator->buffer, LEN, stdin);
}

//=======================================================================================================================

#ifdef __cplusplus
}
#endif

#endif /* CALCULATOR_H */