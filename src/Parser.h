/**
 * Parser.h
 *
 */
#ifndef PARSER_H
#define PARSER_H

#ifdef __cplusplus
extern "C" {
#endif

//=======================================================================================================================

typedef enum {
    NONE = 0,
    NUMBER = 1,
    OPERATOR = 2
} parse_type;

typedef struct {

} Parser;

//=======================================================================================================================

#ifdef __cplusplus
}
#endif

#endif /* PARSER_H */