/**
 * Stack.h
 *
 * Implementation of a stack using a (limited) array
 *
 */
#ifndef STACK_H
#define STACK_H

#ifdef __cplusplus
extern "C" {
#endif

//=======================================================================================================================
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define STACK_API extern
#define SIZE 100 // Default stack size
////
// sizeof gives the amount of storage )in bytes. sizeof(int) gives 4 bytes for every Integer.
// With 4 byes (= 4*8 Bits = 32 Bits) one can represent 2^32 numbers. That are following numbers:
// from 0 until (2^32-1), or, from -(2^31)..0.. until +(2^31-1)
//
#define STACK_NONE -pow(2,(8*(int)sizeof(int))-1) // -2^(8*4-1)

//=======================================================================================================================

typedef struct {
    int * data;
    unsigned int size; // Size of stack
    unsigned int count; // Number of current data in stack
} Stack;

STACK_API void Stack_Init (Stack * stack, unsigned int size);
STACK_API Stack * Stack_Create (unsigned int size);
STACK_API void Stack_Release(Stack * stack);
STACK_API unsigned int Stack_Count (Stack * stack);
STACK_API int Stack_Peek (Stack * stack);
STACK_API int Stack_Top (Stack * stack);
STACK_API int Stack_Pop (Stack * stack);
STACK_API unsigned int Stack_Push (Stack * stack, int value);

/*
 * Stack_Init
 * 
 * Initialize an empty stack. If [size] is 0 (NULL), a stack with default SIZE, e.g. 100,
 * will be initialized. Otherwise, a stack with user-defined size will be initialized.
 */
STACK_API void Stack_Init (Stack * stack, unsigned int size) {
    stack->size = (size == 0) ? SIZE : size;
    stack->data = (int *) malloc(sizeof(int) * stack->size);
    for (int i = 0; i < stack->size; ++i)
        stack->data[i] = STACK_NONE;
    stack->count = 0;
}

/*
 * Stack_Create
 * 
 * Create an (reference of an) empty stack. If [size] is 0 (NULL), a stack with default SIZE,
 * e.g. 100, will be initialized. Otherwise, a stack with user-defined size will be initialized.
 */
STACK_API Stack * Stack_Create (unsigned int size) {
    Stack * stack = (Stack *) malloc(sizeof(Stack));
    Stack_Init(stack, size);
    return stack;
}

/*
 * Stack_Release
 * 
 * Clear the stack. Remove all data. Free allocated memory.
 */
STACK_API void Stack_Release (Stack * stack) {
    if (stack) {
        free(stack->data);
        stack->data = 0;
    }
    stack->size = stack->count = 0;
}

/*
 * Stack_Count
 * 
 * Return the number of data currently being in stack.
 */
STACK_API unsigned int Stack_Count (Stack * stack) {
    return stack->count;
}

/*
 * Stack_Peek
 * 
 * Return the data on the top of the stack without removing it from stack.
 */
STACK_API int Stack_Peek (Stack * stack) {
    int peeked = STACK_NONE;
    if (stack->count == 0) {
        fputs("Warning: function Stack_Peek(): Stack is empty. There's nothing to peek/pop.\n", stdout);
    }
    else { // stack->count > 0
        peeked = stack->data[stack->count-1];
    }
    return peeked;
}

/*
 * Stack_Top
 * 
 * Return the data on the top of the stack without removing it from stack.
 * Similar to Stack_Peek().
 */
STACK_API int Stack_Top (Stack * stack) {
    return Stack_Peek(stack);
}

/*
 * Stack_Pop
 * 
 * Return the data on the top of the stack and remove it from stack.
 */
STACK_API int Stack_Pop (Stack * stack) {
    int peeked = Stack_Peek(stack);
    if (peeked != STACK_NONE) {
        --stack->count;
        stack->data[stack->count] = STACK_NONE;
    }
    return peeked;
}

/*
 * Stack_Push
 * 
 * Put new data (value) on the top of the stack. Return current number of data on stack.
 */
STACK_API unsigned int Stack_Push (Stack * stack, int value) {
    if (stack->count == stack->size)
        fputs("Warning: function Stack_Push(): Stack is full and cannot get more data.\n", stdout);
    else // stack->count < stack->size
        stack->data[stack->count++] = value;
    return stack->count;
}

//=======================================================================================================================

#ifdef __cplusplus
}
#endif

#endif /* STACK_H */